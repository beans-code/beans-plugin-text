package net.beanscode.plugin.text;

import java.io.IOException;

import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryProgress;
import net.beanscode.model.notebook.ReloadPropagator;
import net.beanscode.model.utils.MarkdownUtils;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.StringUtilities;

public class TextEntry extends NotebookEntry {
	
	public static final String META_TEXT_MARKDOWN 	= "META_TEXT_MARKDOWN";
	public static final String META_TEXT_HTML 		= "META_TEXT_HTML";
	
	public TextEntry() {
		super();
	}
	
	public TextEntry(Notebook notebook, String text) {
		super();
		
		setUserId(notebook.getUserId());
		setNotebookId(notebook.getId());
//		setName(name);
		setText(text);
	}
	
	public TextEntry(UUID userId, UUID notebookId, String name, String text) {
		super();
		
		setUserId(userId);
		setNotebookId(notebookId);
		setName(name);
		setText(text);
	}
	
	@Override
	public String getShortDescription() {
		return "Text";
	}
	
	@Override
	public boolean isReloadNeeded() {
		return false;
	}
	
	@Override
	public String getSummary() {
		String tmp = StringUtilities.substringMax(getText(), 0, 100);
		return "Text: " + ((getText() != null && getText().length() > 100) ? tmp + "..." : tmp);
	}
	
	@Override
	public boolean isRunning() {
		return false;
	}
	
	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		// do nothing
		getProgress()
			.ok()
			.save();
		return ReloadPropagator.NONBLOCK;
	}
	
	@Override
	public String getName() {
		return StringUtilities.substringMax(getText(), 0, 100);
	}

	public String getText() {
		return getMetaAsString(META_TEXT_MARKDOWN, null);
	}

	public void setText(String text) {
		setMeta(META_TEXT_MARKDOWN, text);
		setMeta(META_TEXT_HTML, MarkdownUtils.toHtml(getText(), getUserId()));
	}
	
	public String getTextAsHtml() {
		String html = getMetaAsString(META_TEXT_HTML, null);
		if (html == null) {
			html = MarkdownUtils.toHtml(getText(), getUserId());
			setMeta(META_TEXT_HTML, html);
		}
		return html;
	}
	
	public boolean isEmpty() {
		return StringUtilities.nullOrEmpty(getText());
	}
	
	@Override
	public String getEditorClass() {
		return "net.beanscode.plugin.text.TextPanel";
	}
	
	@Override
	public void stop() throws ValidationException, IOException {
		// do nothing	
	}

	@Override
	public void start() throws ValidationException, IOException {
		setMeta(META_TEXT_HTML, MarkdownUtils.toHtml(getText(), getUserId()));
		save();
		
		new NotebookEntryProgress(this)
			.ok()
			.save();
	}
}
