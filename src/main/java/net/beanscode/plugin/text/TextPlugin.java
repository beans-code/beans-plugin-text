package net.beanscode.plugin.text;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.utils.MarkdownUtils;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class TextPlugin implements Plugin {

	public TextPlugin() {
		
	}

	@Override
	public String getName() {
		return "Text Plugin";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}

	@Override
	public String getDescription() {
		return "Text plugin allows to add texts to Notebooks in the Markdown format";
	}

	@Override
	public String getPanel(UUID userId) throws IOException {
		return MarkdownUtils.toHtml(SystemUtils.readFileContent(TextPlugin.class, "text-plugin-description.md"));
	}

	@Override
	public List<Func> getFuncList() {
		return null;
	}

	@Override
	public List<Class<? extends Connector>> getConnectorClasses() {
		return null;
	}

	@Override
	public List<Class<? extends NotebookEntry>> getNotebookEntries() {
		List<Class<? extends NotebookEntry>> entries = new ArrayList<>();
		entries.add(TextEntry.class);
		return entries;
	}
	
	@Override
	public List<Class<? extends DatabaseProvider>> getDatabaseProviderList() {
		return null;
	}
	
	@Override
	public List<Class<? extends SearchEngineProvider>> getSearchEngineProviderList() {
		return null;
	}
	
	@Override
	public boolean selfTest(UUID userId, OutputStreamWriter output) throws ValidationException, IOException {
		final Notebook notebook = new Notebook(userId, "[SelfTest] Test notebook with text entry");
		notebook.save();
		
		output.append("Created notebook " + notebook + "\n");
		
		final TextEntry te = new TextEntry(notebook, "Test content, test **bold** content.");
		te.save();
		te.start();

		output.append("Created text entry " + te.getSummary() + "\n");
		
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			LibsLogger.error(TextEntry.class, "Cannot sleep", e);
		}
		
		final TextEntry teFromDb = (TextEntry) NotebookEntryFactory.getNotebookEntry(te.getId());
		final String expecting = "<p>Test content, test <strong>bold</strong> content.</p>";
		if (teFromDb.getText().equals("Test content, test **bold** content."))
			output.append("Text was saved correctly\n");
		else
			output.append("Text was NOT saved correctly\n");
		if (teFromDb.getTextAsHtml().trim().equals(expecting.trim()))
			output.append("Html text was saved correctly\n");
		else {
			output.append("Html text was NOT saved correctly\n");
			output.append("It was expected to see: " + expecting + "\n");
			output.append("It is: " + te.getTextAsHtml());
		}
		
		return true;
	}
}
