package net.beanscode.plugin.text;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.target;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.cli.notebooks.TextEntry;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.notebook.NotebookEntryEditorFactory;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class TextPanel extends NotebookEntryEditorPanel {
	
	public TextPanel() {
		
	}
	
	public TextPanel(Component parent) {
		super(parent);
	}
	
	public TextPanel(Component parent, TextEntry textEntry) {
		super(parent);
		setNotebookEntry(textEntry);
	}
	
	@Override
	public Renderable getView() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				
				String htmlTxt = getTextEntry() != null ? getTextEntry().getTextAsHtml() : "";
				
				try {
					int c = 0;
					while (RegexUtils.contains("entry:[a-zA-Z\\d]+", htmlTxt)) {
						if (c++ > 100)
							break;
						
						String entryId = RegexUtils.firstGroup("entry:([a-zA-Z\\d]+)", htmlTxt);
						
						NotebookEntry ne = NotebookEntryGateway.getNotebookEntry(getSessionBean(), entryId);
						
						NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(TextPanel.this, ne.getClass());
						nePanel.setNotebookEntry(ne);
//						nePanel.setParams(getParams());
						nePanel.setSessionBean(getSessionBean());
						
						htmlTxt = htmlTxt.replace("entry:" + entryId, new HtmlCanvas().render(nePanel.getView()).toHtml());
					}
				} catch (Exception e) {
					LibsLogger.error(TextPanel.class, "Cannot replace objects with panels", e);
				}
				
				html
					.div()
						.content(htmlTxt, false)
					;
			}
		};
	}
	
	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.input(type("hidden")
						.name(getId() + "-text-text")
						.id(getId() + "-text-text")
						.value(" "))
					
					.textarea(id(getId() + "-text"))
						.content(getTextEntry().getText())
						
					.write(new OpJs("window.myCodeMirror" + getId() + " = CodeMirror.fromTextArea(document.getElementById(\"" + getId() + "-text\"), "
							+ "{ "
							+ "	mode:  \"markdown\", "
							+ "	indentUnit: 4, "
							+ "	lineNumbers: true, "
							+ " theme: \"default\", "
							+ " extraKeys: {\"Enter\": \"newlineAndIndentContinueMarkdownList\"} "
							+ "});").toStringAdhock(), false)
						
					.a(target("_blank").href("https://daringfireball.net/projects/markdown/syntax"))
						.write("markdown help ")
						.i(class_("glyphicon glyphicon-share"))
						._i()
					._a()
					.span()
						.content(", or ")
					.a(target("_blank").href("https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet"))
						.write("another help ")
						.i(class_("glyphicon glyphicon-share"))
						._i()
					._a()
					;
			}
		};
	}

	@Override
	protected OpList onBeforePlay() {
		OpList ops = new OpList();
		
		ops.add(new OpJs("document.getElementById(\"" + getId() + "-text-text\").value = "
				+ "window.myCodeMirror" + getId() + ".getValue(); "));
		
		return ops;
	}
	
	@Override
	protected Button getMenu() throws IOException {
		return null;
	}
	
	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		return new OpList()
//			.add(new OpSet("#" + getId() + " .status", ren new HtmlCanvas().render(getView()).toHtml()))
			.add(new OpSet("#" + getId() + " .content", new HtmlCanvas().render(getView()).toHtml()));
	}
	
	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
//		try {
//			new ApiCallCommand(this)
//				.setApi(BeansCliCommand.API_TEXT_SAVE)
//				.addParam("textId", getTextEntry().getId())
//				.addParam("text", params.getString(getId() + "-text-text"))
//				.run();
			
			new ApiCallCommand(this)
				.setApi(BeansCliCommand.API_NOTEBOOK_ENTRY)
				.setApiCallType(APICallType.POST)
				.addParam("entryId", getTextEntry().getId())
				.addParam(TextEntry.META_TEXT_MARKDOWN, params.getString(getId() + "-text-text"))
				.run();

			// start entry
			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry/start")
				.addParam("entryId", getTextEntry().getId())
				.run();
			
			setNotebookEntry(null);
//			getTextEntry().setText(params.getString("text"));
//			getTextEntry().save();
//		} catch (IOException e) {
//			LibsLogger.error(TextPanel.class, "Cannot save notebook entry", e);
//		}
		
		return new OpList()
			.add(!getTextEntry().isEmpty() ? new OpRemoveClass("#" + getId(), "warn") : new OpAddClass("#" + getId(), "warn"));
	}
	
	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return new OpList();
	}
	
	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		return new OpList();
	}
	
	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(TextPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}
	
//	@Override
//	public OpList getInitOpList() {
//		OpList opList = super.getInitOpList();
//		
//		if (getTextEntry().isEmpty())
//			opList.addOp(new OpAddClass("#" + getId(), "warn"));
//		
//		return opList;
//	}

	public TextEntry getTextEntry() {
		return (TextEntry) getNotebookEntry();
	}
}
